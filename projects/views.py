from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.urls import reverse_lazy

#  IMPORT THINE MODELS!!!!

from tasks.models import Task
from projects.models import Project

# Create your views here.


class ProjectListView(LoginRequiredMixin, ListView):
    model = Project
    template_name = "list_projects.html"  # remember there is an "S" here!
    context_object_name = "listprojects"  # remember there is an "S" here!

    def get_queryset(self):
        queryset = Project.objects.filter(members=self.request.user)
        return queryset

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["projectinfo"] = Task.objects.all()
        return context


# list view


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    template_name = "show_project.html"
    context_object_name = "showproject"
    # extra_context = {'tasks': Task.objects.all()}

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["projectinfo"] = Task.objects.all()
        return context


# create view


class ProjectCreateView(LoginRequiredMixin, CreateView):
    model = Project
    fields = ["name", "description", "members"]
    success_url = reverse_lazy("list_projects")
    # def form_valid(self, form):
    #     item = form.save(commit=False)
    #     print(item)
    #     item.save()
    #     item.members.set(self.request.user) #= self.request.user
    #     item.save_m2m()
    #     return redirect("show_project")
