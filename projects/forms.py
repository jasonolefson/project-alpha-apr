from django import forms
from .models import Project


class ProjectForm(forms.ModelForm):
    class Meta:
        model = Project
        fields = ["name", "description", "members"]

    members = forms.ModelMultipleChoiceField(
        queryset=Project.objects.all(), widget=forms.CheckboxSelectMultiple
    )
