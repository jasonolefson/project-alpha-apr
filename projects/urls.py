from django.urls import path


# import my views
from projects.views import (
    ProjectListView,
    ProjectDetailView,
    ProjectCreateView,
)


# Create URL patterns!

urlpatterns = [
    path("", ProjectListView.as_view(), name="list_projects"),
    path("<int:pk>/", ProjectDetailView.as_view(), name="show_project"),
    path("create/", ProjectCreateView.as_view(), name="create_project"),
]
