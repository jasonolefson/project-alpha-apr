from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm

# Create your views here.


def login_page(request):

    return render(request, "accounts/login.html")


def signup(request):
    if request.method != "POST":
        form = UserCreationForm()
    else:
        form = UserCreationForm(request.POST)
        if form.is_valid():
            user = form.instance
            print(user)
            form.save()
            return redirect("home")

    context = {"form": form}

    return render(request, "registration/signup.html", context)
