from django.views.generic.edit import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.urls import reverse_lazy
from django.views.generic.list import ListView

# Create your views here.
from .models import Task


class TaskCreateView(LoginRequiredMixin, CreateView):
    model = Task
    fields = ["name", "start_date", "due_date", "project"]
    success_url = reverse_lazy("list_projects")


class TaskListView(LoginRequiredMixin, ListView):
    model = Task
    template_name = "list_tasks.html"
    context_object_name = "listtasks"


class TaskUpdateView(LoginRequiredMixin, UpdateView):
    model = Task
    template_name = "task_update.html"
    fields = ["is_completed"]
    success_url = reverse_lazy("show_my_tasks")
